<!--
 IMPORTANT: Please use the following link to create a new issue:

  https://watheialabs.com/new-issue/nextjs-material-kit-pro

**If your issue was not created using the app above, it will be closed immediately.**
-->

<!--
Love Watheia Labs? Do you need Angular, React, Vuejs or HTML? You can visit:
👉  https://watheialabs.com/bundles
👉  https://watheialabs.com
-->
